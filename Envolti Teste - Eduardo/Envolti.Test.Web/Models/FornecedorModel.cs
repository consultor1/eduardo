﻿namespace Envolti.Test.Web.Models
{
    public class FornecedorModel
    {
        public long Id { get; set; }
        public string Descricao { get; set; }
    }
}
