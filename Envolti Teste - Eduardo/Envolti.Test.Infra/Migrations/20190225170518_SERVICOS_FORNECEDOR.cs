﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Envolti.Test.Infra.Migrations
{
    public partial class SERVICOS_FORNECEDOR : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE VIEW SERVICOS_FORNECEDOR AS
                SELECT SUM(SERVICO_PRESTADO.Valor) TOTAL, FORNECEDOR.NOME, MONTH(SERVICO_PRESTADO.DataAtendimento) MES FROM SERVICO_PRESTADO 
                	INNER JOIN FORNECEDOR on FORNECEDOR.Id = SERVICO_PRESTADO.FornecedorId
                GROUP BY SERVICO_PRESTADO.FornecedorId, FORNECEDOR.Nome, MONTH(SERVICO_PRESTADO.DataAtendimento);
            ");
        }
    }
}
