﻿using Envolti.Test.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace Envolti.Test.Web.Models
{
    public class ServicosPrestadosModel
    {
        public long Id { get; set; }

        public string Descricao { get; set; }

        public DateTime? DataAtendimento { get; set; }

        public decimal? Valor { get; set; }

        public long IdTipoServico { get; set; }
        public long IdCliente { get; set; }

        public IEnumerable<SelectListItem> TiposServico { get; set; }
        public IEnumerable<SelectListItem> Clientes { get; set; }

        internal ServicosPrestadosEntity ToEntity()
        {
            return new ServicosPrestadosEntity
            {
                Descricao = this.Descricao,
                DataAtendimento = this.DataAtendimento ?? DateTime.Now,
                Valor = this.Valor ?? 0,
                TipoServico = new TipoServicoEntity { Id = this.IdTipoServico },
                Cliente = new ClienteEntity { Id = this.IdCliente }
            };
        }
    }
}
