﻿using Envolti.Test.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace Envolti.Test.Web.Models.Extensions
{
    public static class FornecedorExtensions
    {
        public static IEnumerable<FornecedorModel> ToModel(this IEnumerable<FornecedorEntity> fornecedores)
            => fornecedores?.Select(item => new FornecedorModel
            {
                Id = item.Id,
                Descricao = item.Nome
            });

        public static IEnumerable<SelectListItem> ToListItem(this IEnumerable<FornecedorEntity> fornecedores)
            => fornecedores?.Select(item => new SelectListItem
            {
                Value = item.Id.ToString(),
                Text = item.Nome
            })
            .ToList();

    }
}
