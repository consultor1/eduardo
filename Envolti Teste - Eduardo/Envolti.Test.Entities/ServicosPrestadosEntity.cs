﻿using System;

namespace Envolti.Test.Entities
{
    public class ServicosPrestadosEntity: EntityBase
    {
        public string Descricao { get; set; }
        public DateTime DataAtendimento { get; set; }
        public decimal Valor { get; set; }
        public TipoServicoEntity TipoServico { get; set; }
        public ClienteEntity Cliente { get; set; }
        public FornecedorEntity Fornecedor { get; set; }
    }
}
