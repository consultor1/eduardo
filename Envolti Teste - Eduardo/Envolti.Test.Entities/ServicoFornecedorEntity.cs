﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Envolti.Test.Entities
{
    public class ServicoFornecedorEntity
    {
        public decimal Valor { get; set; }
        public string Fornecedor { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
    }
}
