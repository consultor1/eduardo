﻿using System;

namespace Envolti.Test.Web.Models
{
    public class ServicosPrestadosItemModel
    {
        public long  Id { get; set; }

        public string Descricao { get; set; }

        public DateTime DataAtendimento { get; set; }

        public decimal Valor { get; set; }

        public string TipoServico { get; set; }
    }
}
