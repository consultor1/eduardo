﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Envolti.Test.Infra.Migrations
{
    public partial class Seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("FORNECEDOR", new[] { "Nome" }, new[] { "Fornecedor 1" });
            migrationBuilder.InsertData("FORNECEDOR", new[] { "Nome" }, new[] { "Fornecedor 2" });
            migrationBuilder.InsertData("FORNECEDOR", new[] { "Nome" }, new[] { "Fornecedor 3" });
            migrationBuilder.InsertData("FORNECEDOR", new[] { "Nome" }, new[] { "Fornecedor 4" });

            migrationBuilder.InsertData("CLIENTE",
                new[] { "Nome", "Bairro", "Cidade", "Estado" },
                new[] { "Cliente 1", "Centro", "Blumenau", "SC" });

            migrationBuilder.InsertData("CLIENTE",
                new[] { "Nome", "Bairro", "Cidade", "Estado" },
                new[] { "Cliente 2", "Itoupava Seca", "Blumenau", "SC" });

            migrationBuilder.InsertData("CLIENTE",
                new[] { "Nome", "Bairro", "Cidade", "Estado" },
                new[] { "Cliente 3", "Centro", "Curitiba", "PR" });

            migrationBuilder.InsertData("CLIENTE",
                new[] { "Nome", "Bairro", "Cidade", "Estado" },
                new[] { "Cliente 4", "Centro", "Florianopolis", "SC" });

            migrationBuilder.InsertData("TIPO_SERVICO", new[] { "Nome" }, new[] { "Conserto eletrônico" });
            migrationBuilder.InsertData("TIPO_SERVICO", new[] { "Nome" }, new[] { "Serviços gerais" });
            migrationBuilder.InsertData("TIPO_SERVICO", new[] { "Nome" }, new[] { "Manutenção hidráulica" });
            migrationBuilder.InsertData("TIPO_SERVICO", new[] { "Nome" }, new[] { "Instalação elétrica" });
            migrationBuilder.InsertData("TIPO_SERVICO", new[] { "Nome" }, new[] { "Jardinagem" });

            migrationBuilder.InsertData("SERVICO_PRESTADO",
                new[] { "Descricao", "DataAtendimento", "Valor", "TipoServicoId", "ClienteId", "FornecedorId" },
                new[] { "Descrição do Serviço", "2019-01-01", "50.00", "1", "1", "1" });

            migrationBuilder.InsertData("SERVICO_PRESTADO",
                new[] { "Descricao", "DataAtendimento", "Valor", "TipoServicoId", "ClienteId", "FornecedorId" },
                new[] { "Descrição do Serviço", "2019-01-10", "500.00", "2", "1", "2" });

            migrationBuilder.InsertData("SERVICO_PRESTADO",
                new[] { "Descricao", "DataAtendimento", "Valor", "TipoServicoId", "ClienteId", "FornecedorId" },
                new[] { "Descrição do Serviço", "2019-02-01", "150.00", "3", "2", "3" });

            migrationBuilder.InsertData("SERVICO_PRESTADO",
                new[] { "Descricao", "DataAtendimento", "Valor", "TipoServicoId", "ClienteId", "FornecedorId" },
                new[] { "Descrição do Serviço", "2019-03-01", "20.00", "4", "2", "4" });

            migrationBuilder.InsertData("SERVICO_PRESTADO",
                new[] { "Descricao", "DataAtendimento", "Valor", "TipoServicoId", "ClienteId", "FornecedorId" },
                new[] { "Descrição do Serviço", "2019-02-15", "155.00", "5", "3", "1" });

            migrationBuilder.InsertData("SERVICO_PRESTADO",
                new[] { "Descricao", "DataAtendimento", "Valor", "TipoServicoId", "ClienteId", "FornecedorId" },
                new[] { "Descrição do Serviço", "2019-03-08", "3500.00", "1", "4", "2" });

            migrationBuilder.InsertData("SERVICO_PRESTADO",
                new[] { "Descricao", "DataAtendimento", "Valor", "TipoServicoId", "ClienteId", "FornecedorId" },
                new[] { "Descrição do Serviço", "2019-04-18", "225.00", "2", "3", "1" });

            migrationBuilder.InsertData("SERVICO_PRESTADO",
                new[] { "Descricao", "DataAtendimento", "Valor", "TipoServicoId", "ClienteId", "FornecedorId" },
                new[] { "Descrição do Serviço", "2019-05-22", "480.00", "5", "4", "1" });

            migrationBuilder.InsertData("SERVICO_PRESTADO",
               new[] { "Descricao", "DataAtendimento", "Valor", "TipoServicoId", "ClienteId", "FornecedorId" },
               new[] { "Descrição do Serviço", "2019-05-12", "480.00", "5", "2", "2" });

            migrationBuilder.InsertData("SERVICO_PRESTADO",
               new[] { "Descricao", "DataAtendimento", "Valor", "TipoServicoId", "ClienteId", "FornecedorId" },
               new[] { "Descrição do Serviço", "2019-05-22", "80.00", "1", "4", "1" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
