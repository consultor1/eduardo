﻿using System;

namespace Envolti.Test.Entities
{
    public class EstatisticaClienteEntity
    {
        public long Id { get; set; }
        public string Cliente { get; set; }
        public int Ano { get; set; }
        public int Mes { get; set; }
        public decimal Valor { get; set; }
    }
}
