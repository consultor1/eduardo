﻿using System;
using Envolti.Test.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Envolti.Test.Infra
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            ServicFornecedorConfig(modelBuilder);
            EstatisticaClienteConfig(modelBuilder);
            UserConfig(modelBuilder);
            FornecedoresConfig(modelBuilder);
            ClientesConfig(modelBuilder);
            ServicosPrestadosConfig(modelBuilder);
            TiposServicoConfig(modelBuilder);
        }

        private void ServicFornecedorConfig(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<ServicoFornecedorEntity>();

            entity.ToTable("SERVICOS_FORNECEDOR");
            entity.Property(v => v.Fornecedor).HasColumnName("NOME");
            entity.Property(v => v.Mes).HasColumnName("MES");
            entity.Property(v => v.Valor).HasColumnName("VALOR");
        }

        private void EstatisticaClienteConfig(ModelBuilder modelBuilder)
        {
            //var view = modelBuilder
            //    .Query<EstatisticaClienteEntity>()
            //    .ToView("ESTATISTICA_CLIENTE");
            var entity = modelBuilder.Entity<EstatisticaClienteEntity>();

            entity.ToTable("ESTATISTICA_CLIENTE");
            entity.HasKey(p => p.Id);
            entity.Property(v => v.Id).HasColumnName("ID");
            entity.Property(v => v.Cliente).HasColumnName("CLIENTE");
            entity.Property(v => v.Mes).HasColumnName("MES");
            entity.Property(v => v.Ano).HasColumnName("ANO");
            entity.Property(v => v.Valor).HasColumnName("TOTAL");
        }

        private void UserConfig(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<UserEntity>();

            entity.Property(p => p.IdFornecedor)
                .HasColumnType("BIGINT");
        }

        private void TiposServicoConfig(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<TipoServicoEntity>();

            entity.ToTable("TIPO_SERVICO");
            entity.HasKey(p => p.Id);
            entity.Property(p => p.Id)
                .IsRequired()
                .HasColumnType("BIGINT");

            entity.Property(p => p.Nome)
                .IsRequired()
                .HasColumnType("VARCHAR(2000)");

        }

        private void ServicosPrestadosConfig(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<ServicosPrestadosEntity>();

            entity.ToTable("SERVICO_PRESTADO");
            entity.HasKey(p => p.Id);
            entity.Property(p => p.Id)
                .IsRequired()
                .HasColumnType("BIGINT");

            entity.Property(p => p.Descricao)
                .IsRequired()
                .HasColumnType("VARCHAR(2000)");

            entity.Property(p => p.DataAtendimento)
                .IsRequired()
                .HasColumnType("DATETIME")
                .HasDefaultValueSql("GetDate()");

            entity.Property(p => p.Valor)
                .IsRequired()
                .HasColumnType("DECIMAL(10,2)");

            entity
                .HasOne(p => p.TipoServico)
                .WithMany()
                .HasPrincipalKey(p => p.Id);

            entity
                .HasOne(p => p.Cliente)
                .WithMany()
                .HasPrincipalKey(p => p.Id);
        }

        private void ClientesConfig(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<ClienteEntity>();

            entity.ToTable("CLIENTE");
            entity.HasKey(p => p.Id);
            entity.Property(p => p.Id)
                .IsRequired()
                .HasColumnType("BIGINT");

            entity.Property(p => p.Nome)
                .IsRequired()
                .HasColumnType("VARCHAR(2000)");

            entity.Property(p => p.Bairro)
                .IsRequired()
                .HasColumnType("VARCHAR(2000)");

            entity.Property(p => p.Cidade)
                .IsRequired()
                .HasColumnType("VARCHAR(2000)");

            entity.Property(p => p.Estado)
                .IsRequired()
                .HasColumnType("VARCHAR(2)");
        }

        private void FornecedoresConfig(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<FornecedorEntity>();

            entity.ToTable("FORNECEDOR");
            entity.HasKey(p => p.Id);
            entity.Property(p => p.Id)
                .IsRequired()
                .HasColumnType("BIGINT");

            entity.Property(p => p.Nome)
                .IsRequired()
                .HasColumnType("VARCHAR(2000)");
        }

        public new DbSet<UserEntity> Users { get; set; }
        public DbSet<FornecedorEntity> Fornecedores{ get; set; }
        public DbSet<ClienteEntity> Clientes { get; set; }
        public DbSet<ServicosPrestadosEntity> ServicosPrestados { get; set; }
        public DbSet<TipoServicoEntity> TiposServico { get; set; }
        public DbSet<EstatisticaClienteEntity> EstatisticaClientes { get; set; }
        public DbSet<ServicoFornecedorEntity> ServicoFornecedor { get; set; }
    }
}
/*
 
     
            migrationBuilder.Sql(@"CREATE VIEW ESTATISTICA_CLIENTE AS
SELECT TOP 3 CLIENTE.ID, CLIENTE.Nome CLIENTE, YEAR(SERVICO_PRESTADO.DataAtendimento) ANO, MONTH(SERVICO_PRESTADO.DataAtendimento) MES, SUM(SERVICO_PRESTADO.Valor) TOTAL
  FROM SERVICO_PRESTADO
 INNER JOIN CLIENTE on SERVICO_PRESTADO.ClienteId = CLIENTE.Id
 GROUP BY CLIENTE.ID,CLIENTE.Nome,YEAR(SERVICO_PRESTADO.DataAtendimento),MONTH(SERVICO_PRESTADO.DataAtendimento)
 ORDER BY 3 DESC, 4 DESC");

     
                migrationBuilder.InsertData("FORNECEDOR", new[] { "Nome" }, new[] { "Local" });
            migrationBuilder.InsertData("FORNECEDOR", new[] { "Nome" }, new[] { "Remoto" });

            migrationBuilder.InsertData("CLIENTE",
                new[] { "Nome", "Bairro", "Cidade", "Estado" },
                new[] { "Cliente 1", "Centro", "Blumenau", "SC" });

            migrationBuilder.InsertData("CLIENTE",
                new[] { "Nome", "Bairro", "Cidade", "Estado" },
                new[] { "Cliente 2", "Itoupava Seca", "Blumenau", "SC" });

            migrationBuilder.InsertData("CLIENTE",
                new[] { "Nome", "Bairro", "Cidade", "Estado" },
                new[] { "Cliente 3", "Centro", "Curitiba", "PR" });

            migrationBuilder.InsertData("CLIENTE",
                new[] { "Nome", "Bairro", "Cidade", "Estado" },
                new[] { "Cliente 4", "Centro", "Florianopolis", "SC" });

            migrationBuilder.InsertData("TIPO_SERVICO", new[] { "Nome" }, new[] { "Conserto eletrônico" });
            migrationBuilder.InsertData("TIPO_SERVICO", new[] { "Nome" }, new[] { "Serviços gerais" });
            migrationBuilder.InsertData("TIPO_SERVICO", new[] { "Nome" }, new[] { "Manutenção hidráulica" });
            migrationBuilder.InsertData("TIPO_SERVICO", new[] { "Nome" }, new[] { "Instalação elétrica" });
            migrationBuilder.InsertData("TIPO_SERVICO", new[] { "Nome" }, new[] { "Jardinagem" });
 */
