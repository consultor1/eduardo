﻿using Envolti.Test.Entities;
using Envolti.Test.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Envolti.Test.Business.Services
{
    public interface IFornecedorService
    {
        Task<IEnumerable<FornecedorEntity>> GetAllAsync();
    }

    public class FornecedorService : IFornecedorService
    {
        private readonly IFornecedorRepository fornecedorRepository;

        public FornecedorService(IFornecedorRepository fornecedorRepository)
        {
            this.fornecedorRepository = fornecedorRepository;
        }

        public Task<IEnumerable<FornecedorEntity>> GetAllAsync()
           => fornecedorRepository.GetAllAsync();
    }
}
