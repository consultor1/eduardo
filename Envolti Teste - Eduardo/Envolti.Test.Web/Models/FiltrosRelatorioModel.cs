﻿using Envolti.Test.Entities;
using System;

namespace Envolti.Test.Web.Models
{
    public class FiltrosRelatorioModel
    {
        public string DataInicial { get; set; }
        public string DataFinal { get; set; }
        public string Cliente { get; set; }
        public string Estado { get; set; }
        public string Bairro { get; set; }
        public string TipoServico { get; set; }
        public string ValorMinimo { get; set; }
        public string ValorMaximo { get; set; }
        public string Cidade { get; set; }

        internal FiltrosRelatorioEntity ToEntity()
        {
            return new FiltrosRelatorioEntity
            {
                Bairro = this.Bairro,
                Cliente = this.Cliente,
                Estado = this.Estado,
                Cidade = this.Cidade,
                TipoServico = this.TipoServico,
                DataInicial = DateTime.TryParse(DataInicial, out var datainicial) ? datainicial : (DateTime?)null,
                DataFinal = DateTime.TryParse(DataFinal, out var datafinal) ? datafinal : (DateTime?)null,
                ValorMaximo = decimal.TryParse(ValorMaximo, out var valorMaximo) ? valorMaximo : (decimal?)null,
                ValorMinimo = decimal.TryParse(ValorMinimo, out var valorMinimo) ? valorMinimo : (decimal?)null
            };
        }
    }
}
