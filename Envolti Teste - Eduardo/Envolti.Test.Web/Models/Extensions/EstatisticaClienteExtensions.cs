﻿using Envolti.Test.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace Envolti.Test.Web.Models.Extensions
{
    public static class EstatisticaClienteExtensions
    {
        private static string GetMonth(int month)
        {
            switch(month)
            {
                case 1:return "Janeiro";
                case 2:return "Fevereiro";
                case 3:return "Março";
                case 4:return "Abril";
                case 5:return "Maio";
                case 6:return "Junho";
                case 7:return "Julho";
                case 8:return "Agosto";
                case 9:return "Setembro";
                case 10:return "Outubro";
                case 11:return "Novembro";
                case 12:return "Dezembro";
            }
            return string.Empty;
        }

        public static IEnumerable<EstatisticaItemModel> ToModel(this IEnumerable<EstatisticaClienteEntity> estatisticas)
            => estatisticas?.Select(item => new EstatisticaItemModel
            {
                Mes = GetMonth(item.Mes),
                Nome = item.Cliente ?? "Sem Clientes",
                Valor = $"R$ {item.Valor.ToString("0.00")}",
            });

        public static IEnumerable<EstatisticaItemModel> ToModel(this IEnumerable<ServicoFornecedorEntity> servicos)
            => servicos?.Select(item => new EstatisticaItemModel
            {
                Mes = GetMonth(item.Mes),
                Nome = item.Fornecedor ?? "Sem Fornecedor",
                Valor = $"R$ {item.Valor.ToString("0.00")}",
            });
    }
}
