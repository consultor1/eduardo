﻿using System;
using Envolti.Test.Business.Services;
using Envolti.Test.Infra;
using Envolti.Test.Infra.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Envolti.Test.IoC
{
    public class Settings
    {
        public void ConfigureDatabase(IServiceCollection services, string connectionString)
        {
            services.AddDbContextPool<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IFornecedorService, FornecedorService>();
            services.AddScoped<IServicoPrestadoService, ServicoPrestadoService>();
            services.AddScoped<ITipoServicoService, TipoServicoService>();
            services.AddScoped<IClienteServico, ClienteServico>();
            
        }

        public void ConfigureRepositories(IServiceCollection services)
        {
            services.AddScoped<IFornecedorRepository, FornecedorRepository>();
            services.AddScoped<IServicoPrestadoRepository, ServicoPrestadoRepository>();
            services.AddScoped<ITipoServicoRepository, TipoServicoRepository>();
            services.AddScoped<IClienteRepository, ClienteRepository>();
        }
    }
}
