﻿namespace Envolti.Test.Entities
{
    public class ClienteEntity : EntityBase
    {
        public string Nome { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
    }
}
