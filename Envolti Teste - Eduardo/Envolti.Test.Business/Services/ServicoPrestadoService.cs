﻿using Envolti.Test.Entities;
using Envolti.Test.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Envolti.Test.Business.Services
{
    public interface IServicoPrestadoService
    {
        IEnumerable<ServicosPrestadosEntity> GetAll();
        Task CreateAsync(ServicosPrestadosEntity entity);
        IEnumerable<ServicosPrestadosEntity> GetFiltered(FiltrosRelatorioEntity filter);
        IEnumerable<EstatisticaClienteEntity> GetEstatisticaClientes();
    }

    public class ServicoPrestadoService : IServicoPrestadoService
    {
        private readonly IServicoPrestadoRepository repository;

        public ServicoPrestadoService(IServicoPrestadoRepository repository)
        {
            this.repository = repository;
        }

        public async Task CreateAsync(ServicosPrestadosEntity entity)
        {
            await repository.CreateAsync(entity);
        }

        public IEnumerable<ServicosPrestadosEntity> GetAll()
        {
            return repository.GetAll();
        }

        public IEnumerable<ServicosPrestadosEntity> GetFiltered(FiltrosRelatorioEntity filter)
        {
            var result = repository.GetForFilter();
            if(filter.DataInicial != null && filter.DataFinal != null)
            {
                result = result.Where(p => p.DataAtendimento >= filter.DataInicial && p.DataAtendimento <= filter.DataFinal);
            }

            if (filter.ValorMinimo != null && filter.ValorMaximo != null)
            {
                result = result.Where(p => p.Valor >= filter.ValorMinimo && p.Valor <= filter.ValorMaximo);
            }

            if (!string.IsNullOrEmpty(filter.TipoServico))
            {
                result = result.Where(p => p.TipoServico.Nome.Contains(filter.TipoServico));
            }

            if (!string.IsNullOrEmpty(filter.Bairro))
            {
                result = result.Where(p => p.Cliente.Bairro.Contains(filter.Bairro));
            }

            if (!string.IsNullOrEmpty(filter.Cliente))
            {
                result = result.Where(p => p.Cliente.Nome.Contains(filter.Cliente));
            }

            if (!string.IsNullOrEmpty(filter.Estado))
            {
                result = result.Where(p => p.Cliente.Estado.Contains(filter.Estado));
            }

            if (!string.IsNullOrEmpty(filter.Cidade))
            {
                result = result.Where(p => p.Cliente.Cidade.Contains(filter.Cidade));
            }

            return result.ToList();
        }

        public IEnumerable<EstatisticaClienteEntity> GetEstatisticaClientes()
        {
            var currentMonth = DateTime.Now.Month;
            var result = repository.GetEstatisticasCliente(DateTime.Now.Year);

            var lastMonth = currentMonth - 3;
            if (result.Count() > 0)
            {
                lastMonth = result.Max(p => p.Mes);
            }

            if(lastMonth < 1)
            {
                lastMonth = 0;
            }

            if(lastMonth < currentMonth)
            {
                var changes = new List<EstatisticaClienteEntity>(result);

                while (lastMonth < DateTime.Now.Month)
                {
                    lastMonth++;
                    changes.Add(new EstatisticaClienteEntity
                    {
                        Mes = lastMonth
                    });

                    return changes
                        .OrderBy(p => p.Mes)
                        .ToList();
                }
            }

            return result;
        }
    }
}
