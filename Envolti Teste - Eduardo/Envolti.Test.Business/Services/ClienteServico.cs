﻿using Envolti.Test.Entities;
using Envolti.Test.Infra.Repositories;
using System.Collections.Generic;

namespace Envolti.Test.Business.Services
{
    public interface IClienteServico
    {
        IEnumerable<ClienteEntity> GetAll();
    }

    public class ClienteServico : IClienteServico
    {
        private readonly IClienteRepository repository;

        public ClienteServico(IClienteRepository repository)
        {
            this.repository = repository;
        }

        public IEnumerable<ClienteEntity> GetAll()
        {
            return repository.GetAll();
        }
    }
}
