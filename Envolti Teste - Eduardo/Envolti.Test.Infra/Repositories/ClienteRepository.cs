﻿using System.Collections.Generic;
using Envolti.Test.Entities;

namespace Envolti.Test.Infra.Repositories
{
    public interface IClienteRepository
    {
        IEnumerable<ClienteEntity> GetAll();
    }

    public class ClienteRepository : IClienteRepository
    {
        private readonly ApplicationDbContext context;

        public ClienteRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<ClienteEntity> GetAll()
        {
            return context.Clientes;
        }
    }
}
