﻿using Microsoft.AspNetCore.Identity;

namespace Envolti.Test.Entities
{
    public class UserEntity : IdentityUser
    {
        [PersonalData]
        public long? IdFornecedor { get; set; }
    }
}
