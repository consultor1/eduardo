﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Envolti.Test.Business.Services;
using Envolti.Test.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Envolti.Test.Web.Models.Extensions;
using Microsoft.AspNetCore.Authorization;

namespace Envolti.Test.Web.Controllers
{
    public class ServicosPrestadosController : Controller
    {
        private readonly IServicoPrestadoService servicosPrestadorService;
        private readonly ITipoServicoService tipoServicoService;
        private readonly IClienteServico clienteServico;
        private readonly IFornecedorService fornecedorService;

        public ServicosPrestadosController(
            IServicoPrestadoService servicosPrestadorService,
            ITipoServicoService tipoServicoService,
            IClienteServico clienteServico,
            IFornecedorService fornecedorService)
        {
            this.servicosPrestadorService = servicosPrestadorService;
            this.tipoServicoService = tipoServicoService;
            this.clienteServico = clienteServico;
            this.fornecedorService = fornecedorService;
        }

        [Authorize]
        [HttpGet]
        public IActionResult Index()
        {
            var list = servicosPrestadorService
                .GetAll()
                .ToItemModel();
            return View(list);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Relatorio()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public IActionResult Create()
        {
            var model = new ServicosPrestadosModel()
            {
                TiposServico = tipoServicoService.GetAll().ToListItem(),
                Clientes = clienteServico.GetAll().ToListItem()
            };

            return View("Edit", model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create(ServicosPrestadosModel model)
        {
            if(ModelState.IsValid)
            {
                var entity = model.ToEntity();
                await servicosPrestadorService.CreateAsync(entity);
                return RedirectToAction("Index", "ServicosPrestados");
            }

            model.TiposServico = tipoServicoService.GetAll().ToListItem();
            return View("Edit", model);
        }

        [Authorize]
        [HttpPost]
        public JsonResult FiltrarRelatorio(FiltrosRelatorioModel model)
        {
            var filter = model?.ToEntity();
            var result = servicosPrestadorService.GetFiltered(filter);
            return new JsonResult(result.ToReportModel());
        }

        [HttpGet]
        public IActionResult Estatisticas()
        {
            var result = new EstatisticaModel
            {
                EstatisticasCliente = servicosPrestadorService
                    .GetEstatisticaClientes()
                    .ToModel() ?? new List<EstatisticaItemModel>(),
                EstatisticasFornecedor = new List<EstatisticaItemModel>()
            };


            return View(result);
        }
    }
}