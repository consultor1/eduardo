﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Envolti.Test.Infra.Migrations
{
    public partial class ESTATISTICA_CLIENTE : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            //Há um erro nessa lógica, deve ser feito um raqueamento, utilizando o hank by o filtro deve filtrar os 3 primeiros
            //Outra forma melhor é utilizar uma tabela destinada a este relatório, esta deve já terá os dados agrupados.
            migrationBuilder.Sql(@"CREATE VIEW ESTATISTICA_CLIENTE AS
SELECT TOP 3 CLIENTE.ID, CLIENTE.Nome CLIENTE, YEAR(SERVICO_PRESTADO.DataAtendimento) ANO, MONTH(SERVICO_PRESTADO.DataAtendimento) MES, SUM(SERVICO_PRESTADO.Valor) TOTAL
  FROM SERVICO_PRESTADO
 INNER JOIN CLIENTE on SERVICO_PRESTADO.ClienteId = CLIENTE.Id
 GROUP BY CLIENTE.ID,CLIENTE.Nome,YEAR(SERVICO_PRESTADO.DataAtendimento),MONTH(SERVICO_PRESTADO.DataAtendimento)
 ORDER BY 3 DESC, 4 DESC");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"drop VIEW ESTATISTICA_CLIENTE");
        }
    }
}
