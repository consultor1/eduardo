﻿using Envolti.Test.Entities;
using Envolti.Test.Infra.Repositories;
using System.Collections.Generic;

namespace Envolti.Test.Business.Services
{
    public interface ITipoServicoService
    {
        IEnumerable<TipoServicoEntity> GetAll();
    }

    public class TipoServicoService : ITipoServicoService
    {
        private readonly ITipoServicoRepository repository;

        public TipoServicoService(ITipoServicoRepository repository)
        {
            this.repository = repository;
        }

        public IEnumerable<TipoServicoEntity> GetAll()
        {
            return repository.GetAll();
        }
    }
}
