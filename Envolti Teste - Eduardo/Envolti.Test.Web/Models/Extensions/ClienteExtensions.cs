﻿using Envolti.Test.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace Envolti.Test.Web.Models.Extensions
{
    public static class ClienteExtensions
    {
        public static IEnumerable<SelectListItem> ToListItem(this IEnumerable<ClienteEntity> clientes)
            => clientes?.Select(item => new SelectListItem
            {
                Value = item.Id.ToString(),
                Text = item.Nome
            })
            .ToList();

    }
}
