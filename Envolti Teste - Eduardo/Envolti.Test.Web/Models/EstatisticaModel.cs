﻿using System.Collections.Generic;

namespace Envolti.Test.Web.Models
{
    public class EstatisticaItemModel
    {
        public string Mes { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
    }

    public class EstatisticaModel
    {
        public IEnumerable<EstatisticaItemModel> EstatisticasCliente { get; set; }
        public IEnumerable<EstatisticaItemModel> EstatisticasFornecedor { get; set; }
    }
}
