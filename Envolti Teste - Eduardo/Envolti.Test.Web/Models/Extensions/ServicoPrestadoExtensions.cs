﻿using Envolti.Test.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace Envolti.Test.Web.Models.Extensions
{
    public static class ServicoPrestadoExtensions
    {
        public static IEnumerable<ServicosPrestadosModel> ToModel(this IEnumerable<ServicosPrestadosEntity> servicos)
            => servicos?.Select(item => new ServicosPrestadosModel
            {
                Id = item.Id,
                Descricao = item.Descricao,
                DataAtendimento = item.DataAtendimento,
                Valor = item.Valor,
                IdTipoServico = item.TipoServico.Id
            });

        public static IEnumerable<ServicosPrestadosItemModel> ToItemModel(this IEnumerable<ServicosPrestadosEntity> servicos)
            => servicos?.Select(item => new ServicosPrestadosItemModel
            {
                Id = item.Id,
                Descricao = item.Descricao,
                DataAtendimento = item.DataAtendimento,
                Valor = item.Valor,
                TipoServico = item.TipoServico.Nome
            });

        public static IEnumerable<ServicosPrestadosReportModel> ToReportModel(this IEnumerable<ServicosPrestadosEntity> servicos)
            => servicos?.Select(item => new ServicosPrestadosReportModel
            {
                Id = item.Id,
                Cliente = item.Cliente?.Nome,
                Bairro = item.Cliente?.Bairro,
                Cidade = item.Cliente?.Cidade,
                Estado = item.Cliente?.Estado,
                TipoServico = item.TipoServico?.Nome,
                Valor = $"R$ {item.Valor.ToString("0.00")}",
                DataAtendimento = item.DataAtendimento.ToString("dd/MM/yyyy")
            });
    }
}
