﻿using System.Collections.Generic;
using System.Linq;
using Envolti.Test.Entities;

namespace Envolti.Test.Infra.Repositories
{
    public interface ITipoServicoRepository
    {
        IEnumerable<TipoServicoEntity> GetAll();
    }

    public class TipoServicoRepository : ITipoServicoRepository
    {
        private readonly ApplicationDbContext context;

        public TipoServicoRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<TipoServicoEntity> GetAll()
        {
            return context.TiposServico.ToList();
        }
    }
}
