﻿using System;

namespace Envolti.Test.Web.Models
{
    public class ServicosPrestadosReportModel
    {
        public long  Id { get; set; }

        public string Cliente { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string TipoServico { get; set; }
        public string Valor { get; set; }
        public string DataAtendimento { get; set; }
    }
}
