﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Envolti.Test.Entities;
using Microsoft.EntityFrameworkCore;

namespace Envolti.Test.Infra.Repositories
{
    public interface 
        IServicoPrestadoRepository
    {
        IEnumerable<ServicosPrestadosEntity> GetAll();
        Task CreateAsync(ServicosPrestadosEntity entity);
        IQueryable<ServicosPrestadosEntity> GetForFilter();
        IEnumerable<EstatisticaClienteEntity> GetEstatisticasCliente(int year);
    }

    public class ServicoPrestadoRepository : IServicoPrestadoRepository
    {
        private readonly ApplicationDbContext context;

        public ServicoPrestadoRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task CreateAsync(ServicosPrestadosEntity entity)
        {
            entity.TipoServico = await context.TiposServico
                .Where(p => p.Id == entity.TipoServico.Id)
                .FirstOrDefaultAsync();

            entity.Cliente = await context.Clientes
                .Where(p => p.Id == entity.Cliente.Id)
                .FirstOrDefaultAsync();

            await this.context.ServicosPrestados.AddAsync(entity);
            await this.context.SaveChangesAsync();
        }

        public IEnumerable<ServicosPrestadosEntity> GetAll()
        {
            return context.ServicosPrestados
                .Include(p=>p.TipoServico)
                .ToList();
        }

        public IEnumerable<EstatisticaClienteEntity> GetEstatisticasCliente(int year)
        {
            return context.EstatisticaClientes
                .Where(p => p.Ano == year)
                .ToList();
        }

        public IQueryable<ServicosPrestadosEntity> GetForFilter()
        {
            return context.ServicosPrestados
                .Include(p=>p.TipoServico)
                .Include(p=>p.Cliente);
        }

    }
}
