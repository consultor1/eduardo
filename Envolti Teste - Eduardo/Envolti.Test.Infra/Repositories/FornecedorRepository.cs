﻿using Envolti.Test.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Envolti.Test.Infra.Repositories
{
    public interface IFornecedorRepository
    {
        Task<IEnumerable<FornecedorEntity>> GetAllAsync();
    }

    public class FornecedorRepository : IFornecedorRepository
    {
        private readonly ApplicationDbContext dbContext;

        public FornecedorRepository(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IEnumerable<FornecedorEntity>> GetAllAsync()
        {
            return dbContext.Fornecedores.ToList();
            //return await Task.Run<IEnumerable<FornecedorEntity>>(
            //    () => dbContext.Fornecedores.ToList());
        }
    }
}
