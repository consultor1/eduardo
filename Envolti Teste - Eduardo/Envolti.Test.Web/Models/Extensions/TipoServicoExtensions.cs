﻿using Envolti.Test.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace Envolti.Test.Web.Models.Extensions
{
    public static class TipoServicoExtensions
    {
        public static IEnumerable<SelectListItem> ToListItem(this IEnumerable<TipoServicoEntity> tipos)
            => tipos?.Select(item => new SelectListItem
            {
                Value = item.Id.ToString(),
                Text = item.Nome
            })
            .ToList();

    }
}
